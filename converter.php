<?php
  class JsonConverter
  {
    //type — тип виджета
      //container — нужен только для размещения в нем других виджетов, в HTML коде может быть представлен тегом div
      //block — блочный виджет с возможностью минимального форматирования (div)
      //text — текстовый блок
      //image — изображение с URLом, указанном по пути payload.image.image
      //button — Ссылка в виде кнопки с текстом из payload.text и аттрибутом href из payload.link.payload

    //payload — контент, зависящий от типа виджета
    //settings — параметры виджета, влияющие на его представление
    //children — массив дочерних виджетов

    // объявление метода
    public function convertJson($json) {
        $result = [];

        $result = json_decode($json);

        return $result;
    }

    public function convertStyle($json) { // на данный момент - преобразовывает параметры в строку для style
        $result = '';

        foreach ($json as $key => $value) {
          // camelCase -> kebab-case
          $key = strtolower(preg_replace(['/([a-z\d])([A-Z])/', '/([^-])([A-Z][a-z])/'], '$1-$2', $key));

          //CSS-тэг для цвета текста - "color". опечатка в оригинале?
          $key = str_replace('text-color', 'color', $key);

          $result .=  $key.':'.$value.'; ';
        }

        return $result;
    }


    public function showConvertResult($input) {
        $output = '';

        //позже можно выделить каждый тип в отдельную функцию
        switch ($input->type) {
          case 'container':
            $output .= '<div class="container" style="'.$this->convertStyle($input->settings).'">';
            if (!empty($input->children)) {
              foreach ($input->children as $children) {
                $output .= $this->showConvertResult($children);
              }
            }
            $output .= '</div>';
          break;

          case 'block':
            $output .= '<div class="block" style="'.$this->convertStyle($input->settings).'">';
            if (!empty($input->children)) {
              foreach ($input->children as $children) {
                $output .= $this->showConvertResult($children);
              }
            }
            $output .= '</div>';
          break;

          case 'text':
            $output .= '<div class="text" style="'.$this->convertStyle($input->settings).'">'.$input->payload->text.'</div>';
          break;

          case 'image':
            $output .= '<img src="'.$input->payload->image->url.'" style="'.$this->convertStyle($input->settings).'">';
          break;

          case 'button':
            //позже можно использовать CSS-классы для тэга <a>, пока что - такой способ
            $output .= '<a style="-webkit-appearance: button; display: inline; text-decoration: none; color: initial; '.$this->convertStyle($input->settings).'" href="'.$input->payload->link->payload.'">'.$input->payload->text.'</a>';
          break;
        }

        return $output;
    }

  }

  echo '<a style="-webkit-appearance: button; display: inline; text-decoration: none; color: initial;" href="/">ВЕРНУТЬСЯ</a>';

  $file = file_get_contents($_FILES['jsonfile']['tmp_name']);
  $converter = new JsonConverter();

  $converted_file = $converter->convertJson($file);

  echo $converter->showConvertResult($converted_file);
?>
